from flask import Flask, render_template
import pymongo

app = Flask(__name__)


@app.route('/')
def index():

    # Connect to the MongoDB database
    client = pymongo.MongoClient('mongodb://dbmongo:27017')

    #selects the database and collection
    db = client['analysis']
    collection = db['analysis']

    #queries for the info
    stuff = collection.find()
    info = stuff[0]
    
    
    max = info['max']
    min = info['min']
    avg = info['avg']

    items = [max,min,avg]
    

    #return index.html with the max, min, and avg
    return render_template('index.html', data=items)
