const express = require('express');
const app = express();
const bodyParser = require('body-parser');
const mysql = require('mysql');

const PORT = 3000;
const HOST = '0.0.0.0';

app.use(bodyParser.urlencoded({ extended: true }));

const connection = mysql.createConnection({
  host: 'db',
  user: 'tempuser',
  password: 'password',
  database: 'data'
});

app.get('/', (req, res) => {
  res.send(`
  <!DOCTYPE html>
  <html lang="en">
    <head>
      <meta charset="UTF-8" />
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
      <meta http-equiv="X-UA-Compatible" content="ie=edge" />
      <link
        rel="stylesheet"
        href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css"
        integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm"
        crossorigin="anonymous"
      />
      <title>Bootstrap Example</title>
    </head>
    <body>
      <div class="container mt-5">
        <div class="row">
          <div class="col-sm-6">
            <a href="http://localhost:5000" class="btn btn-primary btn-block">View the analytics here!</a>
          </div>
          <div class="col-sm-6">
            <a href="http://localhost:5533" class="btn btn-secondary btn-block">Update the data here!</a>
          </div>
        </div>
        <div class="row mt-5">
          <div class="col-sm-12">
            <form action="/" method="post">
              <div class="form-group">
                <input
                  type="text"
                  name="location"
                  class="form-control"
                  placeholder="location"
                />
              </div>
              <div class="form-group">
                <input
                  type="text"
                  name="temperature"
                  class="form-control"
                  placeholder="temperature"
                />
              </div>
              <button type="submit" class="btn btn-success btn-block">Submit</button>
            </form>
          </div>
        </div>
      </div>
    </body>
  </html>
  `);
});

app.post('/', (req, res) => {
  const { location, temperature } = req.body;
  const sql = `INSERT INTO temperature (location, temperature) VALUES ('${location}', '${temperature}')`;
  connection.query(sql, (error, result) => {
    if (error) throw error;
    res.send(`
    <!DOCTYPE html>
    <html>
      <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
      </head>
      <body>
        <div class="container">
          <p class="alert alert-success">Successfully inserted into the database!</p>
          <a class="btn btn-primary" href="http://localhost:5000">View the analytics here!</a>
          <a class="btn btn-warning" href="http://localhost:5533">Update the data here!</a>
          <a class="btn btn-secondary" href="http://localhost:3000">Go back</a>
        </div>
      </body>
    </html>
  `);
  });
});

app.listen(3000, () => {
  console.log(`Running on port ${PORT}`);
});