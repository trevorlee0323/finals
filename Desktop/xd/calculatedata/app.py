from flask import Flask, render_template
import pymongo
import mysql.connector
app = Flask(__name__)


@app.route('/')
def index():

    return render_template('index.html')

@app.route('/run', methods=['POST'])
def run():
        # Connect to the MySQL database
        connect = mysql.connector.connect(
            user='tempuser', password='password', host='db', database='data', auth_plugin='mysql_native_password')
        cursor = connect.cursor()

        # Execute a query to retrieve the data
        select_query = 'SELECT * FROM temperature'
        cursor.execute(select_query)
        data = cursor.fetchall()
        print(data)

        # Calculate the maximum, minimum, and average
        output = []
        for row in data:
            output.append(row[2])

        max_value = int(max(output))
        min_value = int(min(output))
        avg_value = int(sum(output) / len(output))

        print(output)
        print(max_value)


        # Connect to the MongoDB database
        client = pymongo.MongoClient('mongodb://dbmongo:27017')
        db = client['analysis']
        collection = db['analysis']

        # Insert the results into the MongoDB collection
        newmax = { "$set": { "max": max_value} }
        newmin = { "$set": { "min": min_value} }
        newavg = { "$set": { "avg": avg_value} }

        collection.update_one({}, newmax)
        collection.update_one({}, newmin)
        collection.update_one({}, newavg)

        # Close the connections
        cursor.close()
        connect.close()
        client.close()

        return render_template('index.html')